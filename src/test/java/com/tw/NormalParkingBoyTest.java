package com.tw;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Normal parking boy should park the car into parking lots sequentially until all the parking lots are full.
 */
class NormalParkingBoyTest {
    @Test
    void should_parking_in_the_first_parking_lot() {
        ParkingLot firstParkingLot = ParkingLotFactory.createEmptyParkingLot(1);
        ParkingLot secondParkingLot = ParkingLotFactory.createEmptyParkingLot(3);
        ParkingBoy parkingBoy = createParkingBoy();
        parkingBoy.addParkingLot(firstParkingLot, secondParkingLot);

        parkingBoy.park(new Car());

        assertEquals(0, firstParkingLot.getAvailableParkingPosition());
        assertEquals(3, secondParkingLot.getAvailableParkingPosition());
    }

    private static ParkingBoy createParkingBoy() {
        return ParkingBoyFactory.create(ParkingBoyFactory.PARKING_BOY);
    }
}
