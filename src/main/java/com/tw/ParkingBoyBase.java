package com.tw;

import java.util.*;

public abstract class ParkingBoyBase implements ParkingBoy {
    private final List<ParkingLot> parkingLots = new ArrayList<>();
    private String lastErrorMessage;

    @Override
    public void addParkingLot(ParkingLot... parkingLots) {
        this.parkingLots.addAll(Arrays.asList(parkingLots));
    }

    // TODO: You can override methods or add new methods here if you want
    // <-start-
    public ParkingTicket park(Car car) {
        ParkingTicket ticket = null;
        for (ParkingLot parkingLot : parkingLots) {
            ticket = parkingLot.park(car);
        }
        return ticket;
    }

    public Car fetch(ParkingTicket ticket) {
        Car car = null;
        for (ParkingLot parkingLot : parkingLots) {
            car = parkingLot.fetch(ticket);
        }
        return car;
    }

    // --end->

    @Override
    public String getLastErrorMessage() {
        return lastErrorMessage;
    }

    protected void setLastErrorMessage(String errorMessage) { lastErrorMessage = errorMessage; }

    protected List<ParkingLot> getParkingLots() {
        return Collections.unmodifiableList(this.parkingLots);
    }
}
