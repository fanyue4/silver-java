package com.tw;

import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public class SuperSmartParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        ParkingTicket ticket;
        List<ParkingLot> parkingLots = super.getParkingLots();
        TreeMap<Integer, ParkingLot> availableParkingLots = new TreeMap<>();
        for (ParkingLot parkingLot : parkingLots) {
            int availableRate = parkingLot.getAvailableParkingPosition()/parkingLot.getCapacity();
            availableParkingLots.put(availableRate, parkingLot);
        }
        ParkingLot maxParkingLot = availableParkingLots.get(Collections.max(availableParkingLots.keySet()));

        try {
            ticket = maxParkingLot.park(car);
        } catch (ParkingLotFullException e) {
            super.setLastErrorMessage("The parking lot is full.");
            ticket = null;
        }

        return ticket;
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        Car car = null;
        List<ParkingLot> parkingLots = super.getParkingLots();
        for (ParkingLot parkingLot : parkingLots) {
            try {
                car = parkingLot.fetch(ticket);
            } catch (InvalidParkingTicketException e) {
                super.setLastErrorMessage("Unrecognized parking ticket.");
                car = null;
            } catch (NullPointerException e) {
                super.setLastErrorMessage("Please provide your parking ticket.");
                car = null;
            }
        }

        return car;
    }
    // --end->
}
