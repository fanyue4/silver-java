package com.tw;

import java.util.List;

public class NormalParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        ParkingTicket ticket = null;
        List<ParkingLot> parkingLots = super.getParkingLots();
        for (int i = 0; i < parkingLots.size(); i++) {
            ParkingLot iParkingLot = parkingLots.get(i);
            try {
                ticket = iParkingLot.park(car);
                break;
            } catch (ParkingLotFullException e) {
                super.setLastErrorMessage("The parking lot is full.");
                ticket = null;
            }
        }
        return ticket;
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        Car car = null;
        List<ParkingLot> parkingLots = super.getParkingLots();
        for (ParkingLot parkingLot : parkingLots) {
            try {
                car = parkingLot.fetch(ticket);
            } catch (InvalidParkingTicketException e) {
                super.setLastErrorMessage("Unrecognized parking ticket.");
                car = null;
            } catch (NullPointerException e) {
                super.setLastErrorMessage("Please provide your parking ticket.");
                car = null;
            }
        }
        return car;
    }
    // --end->
}
